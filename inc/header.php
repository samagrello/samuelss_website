<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
	<title>
		<?php

			if ($pageTitle) {
			    
			        echo $pageTitle;
			        
			} else { echo "ERROR";}







			//echo $pageTitle;

		?>
		
	</title>

	<!-- Bootstrap 3.3.6 CSS -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google fonts (Open Sans Condensed - 100,300) -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
	
	<!-- Website CSS -->
	<link rel="stylesheet" type="text/css" href="css/main.css">

</head>
<body>

<nav class="navbar navbar-default">

	<div class="container-fluid">

		<div class="navbar-header">
			<button class="navbar-toggle collapsed" aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" type="button">
				<span class="sr-only">Toggle Navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand opsans" href="#">Base</a>
			
		</div>

		<div id="navbar" class="navbar-collapse collapse">
			
			<ul class="nav navbar-nav">

				<li class="active">
					<a href="/asmegas">Inicial</a>
				</li>

				<li class="dropdown">

					<a class="dropdown-toggle" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" href="#">O Spa<span class="caret"></span></a>

					<ul class="dropdown-menu">
						<li>
							<a href="#">Sobre Nós</a>
						</li>
						<li class="divider" role="separator"></li>
						<li class="dropdown-header">Serviços</li>
						<li>
							<a href="/#">Day Spa</a>
						</li>

						<li>
							<a href="/#">Dia da Noiva</a>
						</li>

						<li>
							<a href="/#">Dia do Noivo</a>
						</li>

						<li class="divider" role="separator"></li>

						<li class="dropdown-header">Pacotes</li>

						<li>
							<a href="/#">Pacote 1</a>
						</li>

						<li>
							<a href="/#">Pacote 2</a>
						</li>
						<li>
							<a href="/#">...</a>
						</li>
						<li class="divider" role="separator"></li>

						<li class="dropdown-header">Promoções</li>
						<li>
							<a href="/#">Promoção 1</a>
						</li>

						<li>
							<a href="/#">Promoção 2</a>
						</li>
					</ul>

				</li>

				<li>
					<a href="/#">Novidades</a>
				</li>

				

				<li>
					<a href="/#">Contato</a>
				</li>

				
			</ul>

			<ul class="nav navbar-nav navbar-right" style="display:none;">
				<li class="active">
					<a href="#">Opt 1</span></a>
				</li>
				<li>
					<a href="#">Opt 1</a>
				</li>
				<li>
					<a href="#">Opt 3</a>
				</li>
			</ul>

		</div>
	</div>
</nav>